package factory_method_test.idcard;

import framework.Product;

public class IDCard extends Product{
    private String owner;
    IDCard(String owner) {
        System.out.printf("制作 %s 的ID卡。\n", owner);
        this.owner = owner;
    }
    public void use() {
        System.out.printf("使用 %s 的ID卡。\n", owner);
    }
    public String getOwner() {
        return owner;
    }
}
