package factory_method_test.framework;

public abstract class Product {
    public abstract void use();
}
