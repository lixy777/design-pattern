package idcard

import "fmt"

type IDCard struct {
	owner string
}

func (idc *IDCard) Use() {
	fmt.Printf("使用 %s 的ID卡。\n", idc.owner)
}

func (idc *IDCard) GetOwner() string {
	return idc.owner
}
