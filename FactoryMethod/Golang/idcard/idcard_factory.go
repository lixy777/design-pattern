package idcard

import (
	"framework"
)

type IDCardFactory struct {
	owners []string
}

func (idcf *IDCardFactory) CreateProduct(owner string) framework.Product {
	var product framework.Product
	product.ConcreteProduct = &IDCard{
		owner: owner,
	}
	return product
}

func (idcf *IDCardFactory) RegisterProduct(product framework.Product) {
	idCard, _ := product.ConcreteProduct.(*IDCard)
	owner := idCard.GetOwner()
	idcf.owners = append(idcf.owners, owner)
}

func (idcf *IDCardFactory) GetOwners() []string {
	return idcf.owners
}
