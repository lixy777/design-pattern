package factory_method

import (
	"framework"
	"idcard"
	"testing"
)

func TestFactoryMethod(t *testing.T) {
	var factory framework.Factory
	factory.ConcreteFactory = new(idcard.IDCardFactory)
	card1 := factory.Create("小明")
	card2 := factory.Create("小红")
	card3 := factory.Create("小刚")
	card1.ConcreteProduct.Use()
	card2.ConcreteProduct.Use()
	card3.ConcreteProduct.Use()
	idCardFactory, _ := factory.ConcreteFactory.(*idcard.IDCardFactory)
	t.Log(idCardFactory.GetOwners())
}
