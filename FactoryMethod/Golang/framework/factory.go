package framework

//抽象类中需要子类进行实现的方法
type IConcreteFactory interface {
	CreateProduct(owner string) Product
	RegisterProduct(product Product)
}

//相当于抽象类
type Factory struct {
	ConcreteFactory IConcreteFactory //包含一个真正的实现的引用
}

//抽象类中被子类共同继承的、且已经实现了的方法
func (f *Factory) Create(owner string) Product {
	product := f.ConcreteFactory.CreateProduct(owner)
	f.ConcreteFactory.RegisterProduct(product)
	return product
}
