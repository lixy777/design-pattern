package framework

//抽象类中需要子类进行实现的方法
type IConcreteProduct interface {
	Use()
}

//相当于抽象类
type Product struct {
	ConcreteProduct IConcreteProduct //包含一个真正的实现的引用
}
