package adapter

import "testing"

func TestAdapter(t *testing.T) {
	// TODO 这个地方不知道怎么能用GoLang的方式规避掉Banner的实例化而直接实例化PrintBanner
	var myPrint Print = &PrintBanner{
		&Banner{text: "Hello"},
	}
	myPrint.PrintWeak()
	myPrint.PrintStrong()
}
