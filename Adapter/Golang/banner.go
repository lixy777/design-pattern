package adapter

import "fmt"

type Banner struct {
	text string
}

func (b *Banner) ShowWithParen() {
	fmt.Println("(" + b.text + ")")
}

func (b *Banner) ShowWithAster() {
	fmt.Println("*" + b.text + "*")
}