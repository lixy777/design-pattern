package adapter

type PrintBanner struct {
	*Banner
}

func (pb *PrintBanner) PrintWeak() {
	pb.ShowWithParen()
}

func (pb *PrintBanner) PrintStrong() {
	pb.ShowWithAster()
}