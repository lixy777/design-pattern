package adapter_test1;

public interface Print {
    public abstract void printWeak();
    public abstract void printStrong();
}
