package template_method_test;


public class Main {
    public static void main(String[] args) {
        AbstractDisplay abstractDisplay1 = new CharDisplay('H');
        AbstractDisplay abstractDisplay2 = new StringDisplay("Hello World!");
        abstractDisplay1.display();
        abstractDisplay2.display();
    }
}
