package template_method

//抽象类中需要子类进行实现的方法
type IConcreteDisplay interface {
	Open()
	Print()
	Close()
}

// TODO Golang中暂时没有抽象(abstract)的关键字，这里将接口和有实体的类组合起来，相当于抽象类
type AbstractDisplay struct {
	ConcreteDisplay IConcreteDisplay //包含一个真正的实现的引用
}

//抽象类中被子类共同继承的、且已经实现了的方法
func (ad *AbstractDisplay) Display() {
	ad.ConcreteDisplay.Open()
	for i := 0; i < 5; i++ {
		ad.ConcreteDisplay.Print()
	}
	ad.ConcreteDisplay.Close()
}