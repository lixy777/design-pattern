package template_method

import "fmt"

type CharDisplay struct {
	Ch string
}

func (cd *CharDisplay) Open() {
	fmt.Print("<<")
}

func (cd *CharDisplay) Print() {
	fmt.Print(cd.Ch)
}

func (cd *CharDisplay) Close() {
	fmt.Println(">>")
}