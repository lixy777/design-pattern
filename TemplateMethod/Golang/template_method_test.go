package template_method

import (
	"testing"
)


func TestTemplateMethod(t *testing.T) {
	var abstractDisplay1 AbstractDisplay
	abstractDisplay1.ConcreteDisplay = &CharDisplay{"H"}
	var abstractDisplay2 AbstractDisplay
	abstractDisplay2.ConcreteDisplay = &StringDisplay{"Hello World!", len("Hello World!")}

	abstractDisplay1.Display()
	abstractDisplay2.Display()
}