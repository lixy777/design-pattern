package template_method

import "fmt"

type StringDisplay struct {
	String string
	Width int
}

func (sd *StringDisplay) Open() {
	sd.printLine()
}

func (sd *StringDisplay) Print() {
	fmt.Println("|" + sd.String + "|")
}

func (sd *StringDisplay) Close() {
	sd.printLine()
}

func (sd *StringDisplay) printLine() {
	fmt.Print("+")
	for i := 0; i < sd.Width; i++ {
		fmt.Print("-")
	}
	fmt.Println("+")
}