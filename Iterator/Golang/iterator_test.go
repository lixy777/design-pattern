package iterator

import (
	"testing"
)

func TestIterator(t *testing.T) {
	bookshelf := &BookShelf{
		Books: make([]Book, 0),
	}

	bookshelf.AppendBook(Book{Name: "Around the World in 80 Days"})
	bookshelf.AppendBook(Book{Name: "Bible"})
	bookshelf.AppendBook(Book{Name: "Cinderella"})
	bookshelf.AppendBook(Book{Name: "Daddy-Long-Legs"})

	var aggregate_interface Aggregate = bookshelf

	iterator := aggregate_interface.Iterator()
	for {
		hasNext := iterator.HasNext()
		if hasNext {
			book:= iterator.Next()
			t.Log(book)
		} else {
			break
		}
	}
}
