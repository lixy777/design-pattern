package iterator

type BookShelf struct {
	Books []Book
}

func (bs *BookShelf) GetBookAt(index int) Book {
	return bs.Books[index]
}

func (bs *BookShelf) AppendBook(book Book) {
	bs.Books = append(bs.Books, book)
}

func (bs *BookShelf) GetNumber() int {
	return len(bs.Books)
}

func (bs *BookShelf) Iterator() Iterator {
	bookshelfIterator := &BookShelfIterator{
		bookshelf: *bs,
		index: 0,
	}
	return bookshelfIterator
}