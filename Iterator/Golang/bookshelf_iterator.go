package iterator

type BookShelfIterator struct {
	bookshelf BookShelf
	index     int
}

func (bsi *BookShelfIterator) HasNext() bool {
	return bsi.index < bsi.bookshelf.GetNumber()
}

func (bsi *BookShelfIterator) Next() interface{} {
	book := bsi.bookshelf.GetBookAt(bsi.index)
	bsi.index++
	return book
}