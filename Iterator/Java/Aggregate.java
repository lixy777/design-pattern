package iterator_test;

public interface Aggregate {
    public abstract Iterator iterator();
}
